require 'sinatra'
require 'mongo'
require 'json'

set :port, 8080
set :bind, '0.0.0.0'

bondis_db = Mongo::Client.new([ '127.0.0.1:27017' ], :database => 'bondis').database

get '/frank-says' do
  'Put this in your pipe & smoke it!'
end

post '/locations/:lineaId/:bondiId' do |linea_id, bondi_id|
  request.body.rewind  # in case someone already read it
  data = JSON.parse request.body.read
  # TODO: Chequear que linea_id, bondi_id, lat y lng sean validos
  collection_linea = bondis_db["linea_#{linea_id}"]
  _bondi_id = "#{bondi_id}"
  collection_linea.indexes.create_one( { 'last_data.coords' => '2dsphere' } )
  if (collection_linea.find({ id: _bondi_id }).count == 0)
    collection_linea.insert_one({ id: _bondi_id, data: [] })
  end

  data_to_insert = { coords: [ data['lat'], data['lng'] ] }

  #TODO: Tratar de hacerlo lo mas rapido que se pueda
  collection_linea.update_one(
    { 'id' => _bondi_id }, 
    { '$set' => { 'last_data' => data_to_insert } })
 
  collection_linea.update_one(
    { 'id' => _bondi_id },  
    { '$push' => { 'data' => data_to_insert } })

  "Lat: #{data_to_insert['lat']}, Lng: #{data_to_insert['lng']}, lineaId: #{linea_id}, bondiId: #{bondi_id}"
end

get '/locations/near' do
  #Lets print all the coordinates
  matching_bondis = []
  lat = params['lat'].to_f
  lng = params['lng'].to_f
  distance = (params['distance'] || 1000).to_i
  bondis_db.collections.each do |collection|
    puts "Olis #{collection}"
    collection.find({ 'last_data.coords' =>
                        { "$near" =>
                          { "$geometry" =>
                            { "type" => "Point", "coordinates" => [lat, lng]},
                              "$maxDistance" => distance }}}).each do |doc|
      coords = doc['last_data']['coords']
      bondi_data = {
        'linea' => collection.name[6..-1], #TODO: Extract logic
        'bondi' => doc['id'],
        'lat' => coords[0],
        'lng' => coords[1]
      }
      matching_bondis << bondi_data
    end
  end
  matching_bondis.to_json
end
